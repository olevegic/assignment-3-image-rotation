// main.c
#include <stdio.h>
#include <stdlib.h>

#include "args_helper.h"
#include "bmp_io.h"
#include "file_io.h"
#include "transformations.h"

void cleanup_and_exit(struct image *source, struct image *transformed)
{
    free_image(source);
    free_image(transformed);
    exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
    // Parse command line arguments
    struct args arguments;

    enum args_validate_status argsStatus = get_args(argc, argv, &arguments);

    if (argsStatus != ARGS_OK)
    {
        fprintf(stderr, "Error: Failed to parse command line arguments.\n");
        return EXIT_FAILURE;
    }

    // Read the source image
    struct image sourceImage = {0};
    enum read_status sourseStatus = read_image_file(arguments.in, &sourceImage);

    if (sourseStatus != READ_OK)
    {
        fprintf(stderr, "Error: Failed to read the source image.\n");
        free_image(&sourceImage); // Free allocated memory
        return EXIT_FAILURE;
    }

    // tranforming image
    struct image transformedImage = transform_image(&sourceImage, &arguments);

    // Check the status of the transformation
    if (transformedImage.status != TRANSFORM_OK)
    {
        fprintf(stderr, "Error: Transformation failed.\n");
        cleanup_and_exit(&sourceImage, &transformedImage);
    }

    // Write the rotated image to the output file
    enum write_status writeStatus = write_image_file(arguments.out, &transformedImage);
    if (writeStatus != WRITE_OK)
    {
        fprintf(stderr, "Error: Failed to write the transformed image.\n");
        cleanup_and_exit(&sourceImage, &transformedImage);
    }

    // Free allocated memory only if the write was successful
    free_image(&sourceImage);
    free_image(&transformedImage);

    return EXIT_SUCCESS;
}
