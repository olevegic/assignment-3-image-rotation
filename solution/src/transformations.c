// transformations.c
#include <stdio.h>
#include <stdlib.h>

#include "generic_alg.h"
#include "transformations.h"

// Current settings for angle
#define MIN_ANGLE (-270)
#define MAX_ANGLE 270
#define ANGLE_STEP 90

// Helper functions
static void get_old_indices(size_t *old_i, size_t *old_j, size_t i, size_t j, uint64_t h, uint64_t w, int64_t angle)
{
    int rotations = (int)(angle / 90) % 4;
    rotations = (rotations % 4 + 4) % 4;

    switch (rotations)
    {
    case 1:
        *old_i = j;
        *old_j = w - 1 - i;
        break;
    case 2:
        *old_i = h - 1 - i;
        *old_j = w - 1 - j;
        break;
    case 3:
        *old_i = h - 1 - j;
        *old_j = i;
        break;
    default:
        *old_i = i;
        *old_j = j;
        break;
    }
}

// Rotating
struct image rotate_image(const struct image *source, const struct args *args)
{
    // Create rotated image
    struct image rotated = {
        .width = source->width,
        .height = source->height,
        .data = NULL,
        .status = TRANSFORM_OK};
    rotated.data = malloc(rotated.width * rotated.height * sizeof(struct pixel));

    if (rotated.data == NULL)
    {
        fprintf(stderr, "Failed to allocate memory for rotated image data\n");
        rotated.status = TRANSFORM_FAILED;
        return rotated;
    }

    char *endptr;
    int angle = (int)strtol(args->angle, &endptr, 10);

    // Check if the angle is valid
    if (*endptr != '\0' || endptr == args->angle)
    {
        fprintf(stderr, "Invalid angle\n");
        rotated.status = TRANSFORM_INCORRECT_ANGLE;
        free_image(&rotated); // Free memory
        return rotated;
    }

    if (angle < MIN_ANGLE || angle > MAX_ANGLE ||
        angle % ANGLE_STEP != 0)
    {
        fprintf(stderr, "Invalid angle\n");
        rotated.status = TRANSFORM_INCORRECT_ANGLE;
        free_image(&rotated); // Free memory
        return rotated;
    }

    // Check if rotation angle is 90 or 270 degrees
    int positive_angle = angle + 360;
    if ((positive_angle % 180) == 90)
    {
        // Swap width and height
        rotated.width = source->height;
        rotated.height = source->width;
    }

    for (size_t i = 0; i < rotated.height; ++i)
    {
        for (size_t j = 0; j < rotated.width; ++j)
        {
            // Calculate the index for the current pixel in the rotated image
            size_t index = i * rotated.width + j;

            // Calculate the old (source) indices based on the rotation angle
            size_t old_i, old_j;
            get_old_indices(&old_i, &old_j, i, j, source->height, source->width, angle);

            // Calculate the old index in the source image
            size_t oldIndex = old_i * source->width + old_j;

            // Copy the pixel value from the source image to the rotated image
            rotated.data[index] = source->data[oldIndex];
        }
    }

    return rotated;
}

void free_image(struct image *img)
{
    if (img->data != NULL)
    {
        free(img->data);
        img->data = NULL;
    }
}

const TransformFunction transform_functions[] = {
    [ROTATE] = {rotate_image},
    [GENERIC] = {generic_algorithm},
    // others
};

struct image transform_image(const struct image *source, const struct args *args)
{
    size_t type_index = args->type;

    if (type_index < sizeof(transform_functions) / sizeof(transform_functions[0]))
    {
        return transform_functions[type_index].transform(source, args);
    }
    else
    {
        fprintf(stderr, "Error: Invalid transform type.\n");
        struct image error_image = {.status = TRANSFORM_FAILED};
        return error_image;
    }
}
