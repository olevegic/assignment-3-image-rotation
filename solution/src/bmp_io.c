// bmp_io.c
#include <stdio.h>
#include <stdlib.h>

#include "bmp_io.h"
#include "errors.h"

// Constants
#define BMP_PADDING_SIZE 4
#define BMP_TYPE 0x4D42
#define HEADER_SIZE sizeof(struct bmp_header)
#define BITS_PER_BYTE 8
#define HEADER_BI_PLANES 1

// Function to calculate padding
static uint32_t calculate_padding(size_t width, size_t pixel_size)
{
    return (BMP_PADDING_SIZE - (width * pixel_size) % BMP_PADDING_SIZE) % BMP_PADDING_SIZE;
}

enum read_status from_bmp(FILE *in, struct image *image)
{
    struct bmp_header header;
    if (!in || fread(&header, sizeof(struct bmp_header), 1, in) != 1)
    {
        fprintf(stderr, "Error reading BMP header.\n");
        return READ_INVALID_PICTURE;
    }

    if (header.bfType != BMP_TYPE)
    {
        fprintf(stderr, "Invalid BMP file format.\n");
        return READ_INVALID_PICTURE;
    }

    if (header.biBitCount != 24)
    {
        fprintf(stderr, "Unsupported BMP bit count: %d bits.\n", header.biBitCount);
        return READ_INVALID_PICTURE;
    }

    image->width = header.biWidth;
    image->height = header.biHeight;

    fseek(in, header.bOffBits, SEEK_SET);

    size_t byte_width = header.biWidth * sizeof(struct pixel);
    size_t padding = calculate_padding(header.biWidth, sizeof(struct pixel));

    // Allocate memory for image data
    image->data = malloc(byte_width * header.biHeight);
    if (image->data == NULL)
    {
        fprintf(stderr, "Memory allocation failed.\n");
        return READ_OUT_OF_MEMORY;
    }

    for (size_t i = 0; i < header.biHeight; i++)
    {
        // Read image line
        if (!fread(image->data + i * header.biWidth, byte_width, 1, in))
        {
            free(image->data);
            fprintf(stderr, "Error reading image line.\n");
            return READ_INVALID_PICTURE;
        }
        fseek(in, (long)padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image *image)
{

    if (!out || !image || image->width == 0 || image->height == 0 || image->data == NULL)
    {
        return WRITE_INVALID_PICTURE; // Invalid parameters
    }

    const size_t byte_width = sizeof(struct pixel) * image->width;
    const size_t padding = calculate_padding(image->width, sizeof(struct pixel));
    char zeroes[BMP_PADDING_SIZE] = {0}; //  write padding

    struct bmp_header header = {0};
    header.bfType = BMP_TYPE;
    header.bOffBits = HEADER_SIZE;
    header.bfileSize = header.bOffBits + (byte_width + padding) * image->height;
    header.biSize = HEADER_BI_SIZE;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = HEADER_BI_PLANES;
    header.biBitCount = sizeof(struct pixel) * BITS_PER_BYTE;

    if (!fwrite(&header, HEADER_SIZE, 1, out))
    {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < header.biHeight; i++)
    {
        if (!fwrite(image->data + i * image->width, byte_width, 1, out))
        {
            return WRITE_ERROR;
        }
        if (!fwrite(zeroes, sizeof(char) * padding, 1, out))
        {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}

enum read_status read_bmp_file(const char *filename, struct image *image)
{
    FILE *fp = fopen(filename, "rb");
    if (!fp)
        return READ_OPEN_FILE_ERROR;

    enum read_status status = from_bmp(fp, image);
    fclose(fp);

    return status;
}

enum write_status write_bmp_file(const char *filename, struct image const *image)
{
    FILE *fp = fopen(filename, "wb");
    if (!fp)
        return WRITE_OPEN_FILE_ERROR;

    enum write_status status = to_bmp(fp, image);
    fclose(fp);

    return status;
}
