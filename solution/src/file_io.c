// file_io.c
#include <stdio.h>

#include "file_io.h"
#include "bmp_io.h"

enum file_type determine_file_type(const char *filename)
{
    // can check what type of file is
    printf("File name: %s\n", filename);
    return BMP_FILE;
}

enum read_status read_image_file(const char *filename, struct image *image)
{
    enum file_type type = determine_file_type(filename);
    switch (type)
    {
    case BMP_FILE:
        return read_bmp_file(filename, image);

    default:
        return READ_INVALID_PICTURE; // unsuported format
    }
}

enum write_status write_image_file(const char *filename, const struct image *image)
{
    enum file_type type = determine_file_type(filename);
    switch (type)
    {
    case BMP_FILE:
        return write_bmp_file(filename, image);
    default:
        return WRITE_INVALID_PICTURE; /// unsuported format
    }
}
