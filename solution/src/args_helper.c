// args_helper.c
#include <stdlib.h>

#include "args_helper.h"

#define EXPECTED_ARG_COUNT 4 // Expected number of command - line arguments : input file, output file, transform type, and transform - specific parameter (angle for rotation)

enum args_validate_status get_args(int argc, char **argv, struct args *args)
{
    // Check the number of arguments
    if (argc != EXPECTED_ARG_COUNT)
    {
        return INVALID_ARGS_NUMBER_AV;
    }

    args->in = argv[1];
    args->out = argv[2];
    args->angle = argv[3];

    if (argc == EXPECTED_ARG_COUNT)
    {
        args->type = ROTATE; // for default
    }

    return ARGS_OK;
}
