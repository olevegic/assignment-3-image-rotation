

#include "generic_alg.h"

struct image generic_algorithm(const struct image *source, const struct args *args)
{
    if (args != NULL)
    {
        printf("Using generic algorithm with arguments: %s\n", args->in);
    }
    struct image result = {
        .width = source->width,
        .height = source->height,
        .data = NULL,
        .status = TRANSFORM_OK};

    // something

    return result;
}
