// image.h
#pragma once
#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct pixel
{
    uint8_t b, g, r;
};

// Transformation status
enum transform_status
{
    TRANSFORM_OK,
    TRANSFORM_FAILED,
    TRANSFORM_INCORRECT_ANGLE,
};

struct image
{
    uint64_t width, height;
    struct pixel *data;
    enum transform_status status;
};

#endif
