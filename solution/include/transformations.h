// transformations.h
#pragma once
#ifndef TRANSFORMATIONS_H
#define TRANSFORMATIONS_H

#include "args_helper.h"
#include "image.h"

typedef struct
{
    struct image (*transform)(const struct image *, const struct args *);
} TransformFunction;

// Function prototypes
struct image rotate_image(const struct image *source, const struct args *args);
struct image transform_image(const struct image *source, const struct args *args);

void free_image(struct image *img);
#endif
