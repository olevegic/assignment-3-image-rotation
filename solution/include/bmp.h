// bmp.h
#pragma once
#ifndef BMP_H
#define BMP_H

#define BMP_TYPE 0x4D42

#include <stdint.h>

#pragma pack(push, 1)
// BMP header
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

//   uint16_t bfType;
//   uint32_t bfileSize;
//   uint32_t bfReserved;
//   uint32_t bOffBits;
// 2 + 4 + 4 + 4 = 14

#define HEADER_BI_SIZE (sizeof(struct bmp_header) - 14)
#endif
