// bpm_io.h
#pragma once
#ifndef BMP_IO_H
#define BMP_IO_H

#include <stdio.h>

#define HEADER_SIZE sizeof(struct bmp_header)

#include "bmp.h"
#include "errors.h"
#include "image.h"

enum read_status from_bmp(FILE *in, struct image *image);
enum write_status to_bmp(FILE *out, const struct image *image);

enum read_status read_bmp_file(const char *filename, struct image *image);
enum write_status write_bmp_file(const char *filename, struct image const *image);
#endif
