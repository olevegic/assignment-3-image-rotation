// args_helper.h
#pragma once
#ifndef ARGS_HELPER_H
#define ARGS_HELPER_H

#include "errors.h"

enum transform_type
{
    ROTATE,
    GENERIC,
};

struct rotate_args
{
    int placeholder; // example
};

struct generic_args
{
    int placeholder; // example
};

struct args
{
    enum transform_type type;
    union
    {
        struct rotate_args rotate_args;
        struct generic_args generic_args;
        // Add more as needed
    } algo_args;

    char *in;
    char *out;
    char *angle;
};

enum args_validate_status get_args(int argc, char **argv, struct args *args);
#endif
