// errors.h
#pragma once
#ifndef ERRORS_H
#define ERRORS_H
#include <stdio.h>

enum args_validate_status
{
    ARGS_OK = 0,
    INVALID_ARGS_NUMBER_AV,
    INVALID_ANGLE_AV,
    INVALID_TRANSFORM_TYPE,
};

enum read_status
{
    READ_OPEN_FILE_ERROR,
    READ_ERROR,
    READ_OK,
    READ_INVALID_PICTURE,
    READ_OUT_OF_MEMORY,
};

enum write_status
{
    WRITE_OPEN_FILE_ERROR,
    WRITE_ERROR,
    WRITE_OK,
    WRITE_INVALID_PICTURE,

};

enum error
{
    OK = 0,
    INVALID_PICTURE,
    OUT_OF_MEMORY,
    INVALID_ARGS_NUMBER_ERROR,
    INVALID_ANGLE_ERROR,
    CLOSE_FILE_ERROR,
};

#endif
