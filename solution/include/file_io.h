// file_io.h
#pragma once
#ifndef FILE_IO_H
#define FILE_IO_H

#include "bmp.h"
#include "errors.h"
#include "image.h"

enum file_type
{
    BMP_FILE,
    UNKNOWN_FILE,
    // other formats
};
enum file_type determine_file_type(const char *filename);

// Function prototypes
enum read_status read_image_file(const char *filename, struct image *image);
enum write_status write_image_file(const char *filename, const struct image *image);

#endif
