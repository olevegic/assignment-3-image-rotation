#pragma once
#ifndef GENERIC_ALG_H
#define GENERIC_ALG_H

#include "args_helper.h"
#include "image.h"

struct image generic_algorithm(const struct image *source, const struct args *args);

#endif
